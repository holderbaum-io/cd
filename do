#!/usr/bin/env bash

set -eu -o pipefail

if [ -n "${CI:-}" ];
then
  VERSION="${CI_PIPELINE_ID}"
else
  VERSION="local"
fi

task_usage() {
  echo "usage: $0 build | package"
  exit 1
}

task_build() {
  mkdir -p build/
  cat >build/index.html <<-EOF
<html>
<head></head>
<body>
Date:    $(date)
Version: $VERSION
</body>
</html>
EOF
}

task_package() {
  mkdir -p release
  (
    cd build
    tar czf "../release/release-$VERSION.tgz" .
  )
}

cmd="task_${1:-}"
shift || true

if type "$cmd" &>/dev/null; then
  "$cmd" "$@"
else
  task_usage
fi
